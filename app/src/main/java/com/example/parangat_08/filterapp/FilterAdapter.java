package com.example.parangat_08.filterapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {
    private ArrayList<String> mFilterName;
    private Bitmap mBitmap,imageOne;
    private Context mContext;


    public FilterAdapter(ArrayList<String> mFilterName, Context mContext) {
        this.mFilterName = mFilterName;
        this.mContext = mContext;
        mBitmap = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.dog);
        imageOne = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
       // imageOne = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.dog);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_row,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        //Bitmap imageOne = BitmapFactory.decodeResource(getResources(), R.drawable.imageOne).copy(Bitmap.Config.ARGB_8888,true);
        // Decoding the image two resource into a Bitmap
        Bitmap imageTwo= BitmapFactory.decodeResource(mContext.getResources(), Constants.Image_filters[i]);
        // Here we construct the canvas with the specified bitmap to draw onto
        Canvas canvas=new Canvas(imageOne);
        canvas.drawBitmap(imageTwo,(imageOne.getWidth()),(imageOne.getHeight()),new Paint());
        viewHolder.filter_image.setImageBitmap(imageOne);
        viewHolder.filter_name_txt.setText(mFilterName.get(i));
    }

    @Override
    public int getItemCount() {
        return mFilterName.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView filter_image;
        private TextView filter_name_txt;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            filter_image  = itemView.findViewById(R.id.filter_image);
            filter_name_txt = itemView.findViewById(R.id.filter_name_txt);
        }
    }
}
