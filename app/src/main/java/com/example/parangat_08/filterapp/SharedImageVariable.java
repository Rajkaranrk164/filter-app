package com.example.parangat_08.filterapp;

import android.graphics.Bitmap;

public class SharedImageVariable {
    public static Bitmap mBitmap;
    public static void setBitmap(Bitmap bitmap){
        mBitmap = bitmap;
    }
    public static Bitmap getBitmap(){
        return mBitmap;
    }
}
