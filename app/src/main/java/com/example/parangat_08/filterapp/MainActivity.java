package com.example.parangat_08.filterapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ImageView mImageView;
    private RecyclerView mFilterList;
    private ArrayList<String> mFilterNames;
    private FilterAdapter mFilterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = findViewById(R.id.image_view);
        mFilterList = findViewById(R.id.filter_list);
        setRecyclerView();
    }

    private  void setRecyclerView(){
        mFilterList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.setReverseLayout(false);
        mFilterList.setLayoutManager(layoutManager);
        populateFilter();
        mFilterList.setAdapter(mFilterAdapter);
    }

    private void populateFilter(){
        mFilterNames = new ArrayList<>();
        for (int i=0;i<10;i++){
            mFilterNames.add("Filter "+(i+1));
        }
        mFilterAdapter = new FilterAdapter(mFilterNames,this);
    }
}
